package com.itau.perguntas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.itau.perguntas.service.PerguntaSender;


@ComponentScan({"com.itau.perguntas.config", "com.itau.perguntas.controller", "com.itau.perguntas.model", "com.itau.perguntas.repository",
	 "com.itau.perguntas.service"})
@SpringBootApplication
public class PerguntasApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(PerguntasApp.class, args);
    }
}
