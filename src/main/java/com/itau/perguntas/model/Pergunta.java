package com.itau.perguntas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Pergunta implements Serializable{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idPergunta; 
	
	private String title; 
	
	private String category;
    
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	
	private int answer;
	
	

	public long getIdPergunta() {
		return idPergunta;
	}

	public void setIdPergunta(long idPergunta) {
		this.idPergunta = idPergunta;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}
//
//	@Override
//	public String toString() {
//		return "Pergunta [idPergunta=" + idPergunta + ", title=" + title + ", category=" + category + ", option1="
//				+ option1 + ", option2=" + option2 + ", option3=" + option3 + ", option4=" + option4 + ", answer="
//				+ answer + "]";
//	}

	
	
//	@Override
//	public String toString() {
//		return "Pergunta {\"idPergunta\"=" + idPergunta + ""\"title\"=" + "\""+ title + "\""+", \"category\"=\"" + category + "\", \"option1\"="
//				+ option1 + "\", \"option2\"=" + option2 + "\", \"option3\"= \"" + option3 + "\", \"option4\"=\"" + option4 + "\", \"answer\"=\""
//				+ answer + "\"}";
//	}
}
