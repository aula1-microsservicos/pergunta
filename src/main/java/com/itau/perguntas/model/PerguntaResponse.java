package com.itau.perguntas.model;

import java.util.ArrayList;

public class PerguntaResponse {
	
	private ArrayList<Pergunta> questoes;

	public ArrayList<Pergunta> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(ArrayList<Pergunta> questoes) {
		this.questoes = questoes;
	}
	

}
