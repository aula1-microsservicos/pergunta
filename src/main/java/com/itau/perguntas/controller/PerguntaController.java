package com.itau.perguntas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.itau.perguntas.repository.PerguntaRepository;
import com.itau.perguntas.service.PerguntaSender;
import com.mysql.fabric.xmlrpc.base.Array;

import java.util.ArrayList;
import java.util.List;

import com.itau.perguntas.model.Pergunta;
import com.itau.perguntas.model.PerguntaResponse;



@RestController
@RequestMapping("/pergunta")
@CrossOrigin
public class PerguntaController {



	@Autowired private PerguntaRepository perguntaRepository;
	@Autowired private PerguntaSender perguntaSender;
	@RequestMapping(method=RequestMethod.GET, path="/obterperguntas/{numeroPerguntas}/{categoria}")
	public ResponseEntity<String> obtemPerguntas (@PathVariable int numeroPerguntas, @PathVariable String categoria) {


		List <Pergunta> listaPergunta = perguntaRepository.recuperaPerguntas(categoria); //, categoria);
		List <Pergunta> resultado = new ArrayList<>();
		if(listaPergunta.size() < numeroPerguntas) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Número de perguntas insuficiente. Solicite adiçao de perguntas");
		}
		else {
			for(int i=0; i < numeroPerguntas; i++) {
				resultado.add(listaPergunta.get(i));
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(resultado.toString());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/obtemperguntas/{numeroPerguntas}")
	public ResponseEntity<List<Pergunta>> obtem_Perguntas (@PathVariable int numeroPerguntas) {

		System.out.println("pasosu por aqui");
		List <Pergunta> listaPergunta = perguntaRepository.recuperaPerguntas(); //, categoria);
		
		ArrayList <Pergunta> resultado = new ArrayList<>();
		if(listaPergunta.size() < numeroPerguntas) {
			//return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Número de perguntas insuficiente. Solicite adiçao de perguntas");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		else {
			for(int i=0; i < numeroPerguntas; i++) {
				resultado.add(listaPergunta.get(i));
			}
		}
		

		return ResponseEntity.status(HttpStatus.OK).body(resultado);
	}

	@RequestMapping(method=RequestMethod.GET, path="insere")
	public ResponseEntity<List<Pergunta>> insere(){
		String url = "http://10.162.106.158:8000/all";
		
		RestTemplate template = new RestTemplate();

		List<Pergunta> perguntas = new ArrayList<>();

		ParameterizedTypeReference<ArrayList<Pergunta>>  parameterizedTypeReference = 
				new ParameterizedTypeReference<ArrayList<Pergunta>>(){};		    
				
				ResponseEntity<ArrayList<Pergunta>> lista = template.exchange(url, HttpMethod.GET, null, parameterizedTypeReference );	
				perguntas = lista.getBody();
				
				for (Pergunta p : perguntas) {
					perguntaRepository.save(p);
				}
				return ResponseEntity.ok().body(perguntas);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="listar")
	public ResponseEntity<String> listar(){
		
		perguntaSender.listar(10);
		return ResponseEntity.ok().body("Ok");
	}

}
