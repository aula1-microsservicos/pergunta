package com.itau.perguntas.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.itau.perguntas.model.Pergunta;
import com.itau.perguntas.repository.PerguntaRepository;

@Component
public class PerguntaListener {

	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private PerguntaRepository perguntaRepository;

	@JmsListener(destination = "b.queue.questions.full", containerFactory = "questionJmsFactory")
	public void receiveMessage(@Payload Map message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
		
		System.out.println(">>>>" + message + "<<<<<");
		
		jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), new MessageCreator() {
			
			@Override
			public Message createMessage(Session session) throws JMSException {
				
//				MapMessage messageTosend = session.createMapMessage();
//				messageTosend.setJMSCorrelationID((String) jmsMessageHeaderAccessor.getMessageId());
//				System.out.println("Fila de destino: " + jmsMessageHeaderAccessor.getReplyTo());
//				System.out.println("----->>>" + (String) jmsMessageHeaderAccessor.getCorrelationId());
//				System.out.println("----->>>" + (String) jmsMessageHeaderAccessor.getMessageId());
				List<Pergunta> questions = perguntaRepository.recuperaPerguntas();
				List< Map> lista = new ArrayList<>();

				for (Pergunta p : questions) {
					Map<String, String> tmp = new HashMap<>();
					tmp.put("title", p.getTitle());
					tmp.put("category", p.getCategory());
					tmp.put("option1", p.getOption1());
					tmp.put("option2", p.getOption2());
					tmp.put("option3", p.getOption3());
					tmp.put("option4", p.getOption4());
					tmp.put("answer", String.valueOf(p.getAnswer()));
					
					lista.add(tmp);

				}
				

//				System.out.println("Pergunta ->>>>" + messageTosend + " >>>>>> criada ");
//				messageTosend.setObject("questions", lista);
				System.out.println("Pergunta com listaaaa ->>>>" + lista + " >>>>>> criada ");
				
				Message messageTosend = session.createObjectMessage((Serializable) lista);
				messageTosend.setJMSCorrelationID(jmsMessageHeaderAccessor.getCorrelationId());
				return messageTosend;

			}
		});
	}
	
	
	/**
	 * 
	 * @param message
	 * 
	 * MessageCreator msg = new MessageCreator() {

			@Override
			public Message createMessage(Session session) throws JMSException {

				MapMessage messageTosend = session.createMapMessage();
				messageTosend.setJMSCorrelationID((String) jmsMessageHeaderAccessor.getMessageId());
				System.out.println("Fila de destino: " + jmsMessageHeaderAccessor.getReplyTo());
				System.out.println("----->>>" + (String) jmsMessageHeaderAccessor.getCorrelationId());
				System.out.println("----->>>" + (String) jmsMessageHeaderAccessor.getMessageId());
				List<Pergunta> questions = perguntaRepository.recuperaPerguntas();
				List< Map<String, String>> lista = new ArrayList<>();

				for (Pergunta p : questions) {

					HashMap<String, String> tmp = new HashMap<>();
					tmp.put("title", p.getTitle());
					tmp.put("category", p.getCategory());
					tmp.put("option1", p.getOption1());
					tmp.put("option2", p.getOption2());
					tmp.put("option3", p.getOption3());
					tmp.put("option4", p.getOption4());
					tmp.put("answer", String.valueOf(p.getAnswer()));
					
					lista.add(tmp);

				}
				

				System.out.println("Pergunta ->>>>" + messageTosend + " >>>>>> criada ");
				
				messageTosend.setObject("questions", lista);
				return messageTosend;
			}

		};

		jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), msg);

	 */
//	@JmsListener(destination = "sample.pubsub", containerFactory = "pubsub")
//	public void listenTopic(String message) {
//		System.out.println("New pub <" + message + ">");
//	}

}
