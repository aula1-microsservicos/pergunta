package com.itau.perguntas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.itau.perguntas.model.Pergunta;
import com.itau.perguntas.repository.PerguntaRepository;

@Component
public class PerguntaSender {


	@Autowired private JmsTemplate jmsTemplate;
	@Autowired private PerguntaRepository perguntaRepository;

	public List<Pergunta> listar(int numeroPerguntas){

		List <Pergunta> listaPergunta = perguntaRepository.recuperaPerguntas(); //, categoria);

		ArrayList <Pergunta> resultado = new ArrayList<>();
		if(listaPergunta.size() < numeroPerguntas) {
			//return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Número de perguntas insuficiente. Solicite adiçao de perguntas");
			return null;
		}
		else {
			for(int i=0; i < numeroPerguntas; i++) {
				resultado.add(listaPergunta.get(i));
			}
			sendMessage(resultado);
			return resultado;
		}
	}


	public void sendMessage(List<Pergunta> listaPerguntas) {

		for(Pergunta p : listaPerguntas) {
			HashMap<String, String> body = new HashMap<>();
			//body.put("idPergunta", String.valueOf(p.getIdPergunta()));
			body.put("title", p.getTitle());
			body.put("category", p.getCategory() );
			body.put("option1", p.getOption1());
			body.put("option2", p.getOption2());
			body.put("option3", p.getOption3());
			body.put("option4", p.getOption4());
			body.put("answer", String.valueOf(p.getAnswer()));
			jmsTemplate.convertAndSend("b.queue.questions.full", body);
			System.out.println("Pergunta " + String.valueOf(p.getIdPergunta()) + "enviada ");
		}

		

	}

}
