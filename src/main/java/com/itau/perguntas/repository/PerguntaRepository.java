package com.itau.perguntas.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.itau.perguntas.model.Pergunta;

@Repository
public interface PerguntaRepository extends CrudRepository <Pergunta, Long> {
	
	@Query (value = "select p from Pergunta p where p.category = :categoria order by rand()")
	public List <Pergunta> recuperaPerguntas ( @Param ("categoria") String categoria); //@Param("quantidadePerguntas") int quantidadePerguntas	

	@Query (value = "select p from Pergunta p order by rand()")
	public List <Pergunta> recuperaPerguntas (); //@Param("quantidadePerguntas") int quantidadePerguntas
}
